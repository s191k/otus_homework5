import datetime
import logging
import os
import socket
import sys

SOCKET_SERVER_HOST = 'localhost'
SOCKET_SERVER_PORT = 9999
SOCKET_SERVER = (SOCKET_SERVER_HOST, SOCKET_SERVER_PORT)
PACKAGE_SIZE = 1024
MAX_LISTENER_NUMBER = 5

#200 400 404 405
OK_HEADER = b'HTTP/1.x 200 OK\r\n'
NOT_FOUND_HEADER = b'HTTP/1.x 404 Bad Request\r\n'
FORBIDDEN_REQUEST_HEADER = b'HTTP/1.x 403 Forbidden\r\n'
NOT_ALLOWED_REQUEST_HEADER = b'HTTP/1.x 405 Method Not Allowed\r\n'
CONTENT_TYPE_HEADER = b'Content-Type: text/html; charset=UTF-8\r\n'
# BASIC_PAGE = 'http://' + str(SOCKET_SERVER_HOST) + ':' + str(SOCKET_SERVER_PORT) + '/'
SERVER_HEADER = b'Server: Otus Server/1.1(Win64)\r\n'

from http.server import BaseHTTPRequestHandler, SimpleHTTPRequestHandler

def get_content_type(path):
    if path.endswith('/') or path.endswith('.html'):
        return b'Content-Type: text/html; charset=UTF-8\r\n'
    elif path.endswith('.css'):
        return b'Content-Type: text/css\r\n'
    elif path.endswith('.js'):
        return b'Content-Type: application/javascript\r\n'
    elif path.endswith('.jpg') or path.endswith('.jpeg'):
        return b'Content-Type: image/jpeg\r\n'
    elif path.endswith('.png'):
        return b'Content-Type: image/png\r\n'
    elif path.endswith('.gif'):
        return b'Content-Type: image/gif\r\n'
    # elif path.endswith('.swf'): ???
    #     return b'Content-Type: application/javascript\r\n' ???
    elif path.endswith('.ico'): #???
        return b'Content-Type: image/vnd.microsoft.icon\r\n' #???

def send_html_header(user_socket,path, code=200, content_length=1):
    if code == 200:
        user_socket.send(OK_HEADER)
        user_socket.send(b'Date: ' + str(datetime.datetime.now()).encode('utf-8') + b'\r\n') #date_header
        user_socket.send(SERVER_HEADER)
        # user_socket.send('Content‑Length: 111'.encode('utf-8') + b'\r\n') ##узнать длину пакета
        user_socket.send(b'ContentLength: test\r\n') ##узнать длину пакета
        user_socket.send(b'Connection: close\r\n') ## close?
    if code == 400: user_socket.send(NOT_FOUND_HEADER)
    if code == 403: user_socket.send(FORBIDDEN_REQUEST_HEADER)
    if code == 405: user_socket.send(NOT_ALLOWED_REQUEST_HEADER)
    # user_socket.send(CONTENT_TYPE_HEADER)
    user_socket.send(get_content_type(path))
    user_socket.send(b'\r\n')

def cust_html_to_byte(html_str):
    return html_str.encode('utf-8').replace(b'\n',b'').replace(b'\r',b'').replace(b'\t',b'')

def is_path_exist(path_to_page):
    try:
        # print(os.path.join('DOCUMENT_ROOT',path_to_page.replace('')))
        print('DOCUMENT_ROOT' + path_to_page)
        # return os.path.exists(os.path.join('DOCUMENT_ROOT',path_to_page))
        return os.path.exists('DOCUMENT_ROOT' + path_to_page)
    except Exception as ex:
        logging.error('Get exception while finding your page')
        logging.error(ex)

def get_page(user_socket, path_to_page):
    if is_path_exist(path_to_page):
        # print(path_to_page)
        #########
        if path_to_page.endswith('/'): #перекидывать чтобы в браузере совпадал URL /index.html
            result_path = os.path.join('DOCUMENT_ROOT', path_to_page.replace('/', ''), 'index.html')
            print('result path : ', result_path)

            with open(result_path) as html_page:
                # print('send html')
                file_str = html_page.read()
                print(cust_html_to_byte(file_str))
                send_html_header(user_socket,path_to_page, 200)  # Отправляем заголовок
                user_socket.sendall(cust_html_to_byte(file_str))
                return
        #########
        if os.path.isdir(path_to_page):
            # print('это папка')
            path_to_page = os.path.join(path_to_page, 'index.html')
        else:
            print('это просто страница')
        send_html_header(user_socket,path_to_page,200) #Отправляем заголовок
        with open(path_to_page) as html_page:
            # print(cust_html_to_byte(html_page.read()))
            user_socket.sendall(cust_html_to_byte(html_page.read()))
    else:
        send_html_header(user_socket,path_to_page, 400) #Отправляем заголовок


def method_handler(request, user_socket):
    str_data = request.decode('utf-8')
    # print(str_data)
    method, path, type = str_data.split('\n')[0].split()
    if method == "GET" or method == "HEAD":
        get_page(user_socket, path)
    else: #else 405 error
        send_html_header(user_socket,path, 405)
        user_socket.sendall(b'that"s ERROR-request')


#run_server
serv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serv_socket.bind(SOCKET_SERVER)
serv_socket.listen(MAX_LISTENER_NUMBER)
try:
    while True:
        connection, address = serv_socket.accept()
        try:
            # connection, address = serv_socket.accept()
            print("new connection from {address}".format(address=address))
            data = connection.recv(1024) ##Захватить весь запрос
            if data:
                method_handler(request=data, user_socket=connection)
            print('close')
        except Exception as ex:
            logging.info(ex)
        finally:
            connection.close()
except Exception as ex:
    logging.info(ex)
finally:
    serv_socket.close()